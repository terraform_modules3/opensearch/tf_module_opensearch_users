output "users_initial_passwords" {
  description = "cli command: output -json |jq -rc '.users_initial_passwords.value[] | \"\\(.name) \\(.pass)\" '"
  value       = local.initial_passwords
}
