locals {
  initial_passwords = {
    for user in var.users : user.name => {
      name : user.name
      pass : "${random_string.initial_password[user.name].result}-change-me"
    }
    if !contains(keys(local.user_passwords), user.name)
  }

  user_passwords = {
    for user_password in var.user_passwords : user_password.username => user_password.password
  }
}

resource "opensearch_user" "user" {
  for_each = { for user in var.users : user.name => user }

  username      = each.value.name
  description   = each.value.description
  attributes    = each.value.attributes
  backend_roles = each.value.backend_roles

  password = contains(keys(local.user_passwords), each.value.name) ? local.user_passwords[each.value.name] : "${random_string.initial_password[each.value.name].result}-change-me"

  lifecycle {
    ignore_changes = [password]
  }
}

resource "random_string" "initial_password" {
  for_each = { for user in var.users : user.name => user if !contains(keys(local.user_passwords), user.name) }

  length  = 16
  special = false
}
