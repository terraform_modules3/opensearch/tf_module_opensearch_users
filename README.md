# Terraform Opensearch User Module
Creates Opensearch internal users and maps them to roles

## Usage example
```hcl
# main.tf
module "user" {
  source = "git::https://gitlab.com/terraform_modules3/opensearch/tf_module_opensearch_users.git?ref=0.1.0"

  users          = var.users
  user_passwords = var.user_passwords
  # depends_on     = [module.role]
}
```


```hcl
# terraform.tfvars
users = [
  {
    name  = "logger"
    roles = ["editor"]
  },
  {
    name  = "user1@example.com"
    roles = ["viewer"]
  },
  {
    name = "user2@example.com",
    roles = [
      "viewer",
      "monitor_cluster",
    ],
  },
]
```

```hcl
# secrets.auto.tfvars
user_passwords = [
  {
    username = "logger"
    password = "password1"
  },
]
```
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3.1 |
| <a name="requirement_opensearch"></a> [opensearch](#requirement\_opensearch) | >= 2.0.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 3.5.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_opensearch"></a> [opensearch](#provider\_opensearch) | 2.0.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.5.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [opensearch_roles_mapping.mapper](https://registry.terraform.io/providers/opensearch-project/opensearch/latest/docs/resources/roles_mapping) | resource |
| [opensearch_user.user](https://registry.terraform.io/providers/opensearch-project/opensearch/latest/docs/resources/user) | resource |
| [random_string.initial_password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_user_passwords"></a> [user\_passwords](#input\_user\_passwords) | n/a | <pre>list(object({<br>    username = string<br>    password = string<br>  }))</pre> | `[]` | no |
| <a name="input_users"></a> [users](#input\_users) | List of Opensearch users<br>  Example:<br>  users = [<br>    {<br>      name  = "logger"<br>      roles = ["editor"]<br>    },<br>    {<br>      name  = "user1@example.com"<br>      roles = ["viewer"]<br>    },<br>    {<br>      name = "user2@example.com",<br>      roles = [<br>        "viewer",<br>        "monitor\_cluster",<br>      ],<br>    },<br>  ] | <pre>list(object({<br>    name          = string<br>    description   = optional(string)<br>    attributes    = optional(map(string))<br>    backend_roles = optional(set(string))<br>    roles         = optional(set(string), [])<br>  }))</pre> | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_users_initial_passwords"></a> [users\_initial\_passwords](#output\_users\_initial\_passwords) | cli command: output -json \|jq -rc '.users\_initial\_passwords.value[] \| "\(.name) \(.pass)" ' |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
