variable "users" {
  description = <<EOT
  List of Opensearch users
  Example:
  users = [
    {
      name  = "logger"
      roles = ["editor"]
    },
    {
      name  = "user1@example.com"
      roles = ["viewer"]
    },
    {
      name = "user2@example.com",
      roles = [
        "viewer",
        "monitor_cluster",
      ],
    },
  ]
  EOT
  type = list(object({
    name          = string
    description   = optional(string)
    attributes    = optional(map(string))
    backend_roles = optional(set(string))
    roles         = optional(set(string), [])
  }))
  default = []
}

variable "user_passwords" {
  # description = "value"
  type = list(object({
    username = string
    password = string
  }))
  default = []
}
