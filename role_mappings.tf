locals {
  roles_list = toset(flatten([
    for user in var.users : user.roles
  ]))
}

resource "opensearch_roles_mapping" "mapper" {
  for_each = local.roles_list

  role_name = each.value
  users = [
    for user in var.users : user.name
    if contains(user.roles, each.value)
  ]

  and_backend_roles = []
  backend_roles     = []
  hosts             = []
}
